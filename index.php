<?php

use App\Controllers\NoRouteController;
use App\Controllers\ServerErrorController;
use App\Exceptions\PageNotFoundException;
use App\Exceptions\InternalServerErrorException;
use App\Routers\Router;

require 'vendor/autoload.php';

define('APP_ROOT', dirname(__FILE__));

try {
    $requestPath = $_SERVER['REQUEST_URI'] ?? '';

    $router = new Router();
    $controller = $router->processRequest($requestPath);
    echo $controller->execute();
} catch (PageNotFoundException $pageNotFoundException) {
    $noRoute = new NoRouteController();
    $noRoute->execute();
} catch (InternalServerErrorException $serverErrorException) {
    $serverError = new ServerErrorController();
    $serverError->execute();
} catch (Exception $exception) {
    echo 'Something got wrong: ' . $exception->getMessage() . PHP_EOL;
}
