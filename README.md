# 7. Service Center
## Option 1
### Entities:
1.Service center (name, country, city, street, house, number of places for a car)  
2.Master (name, surname) - serves the car. He can have a senior master, he can also be the senior for another master 
(the number of levels of the hierarchy is not limited).  
3.Car (mileage, number, make, model, country, customer, year of manufacture).
4.Client (name, surname, passport number).  
5.Service registration (master, car, client, date) - linked to the car and the center.

# App for services

Show the list of services, masters, brands and service records stored in the database, also allows you to view 
information about a specific service. You can add a new user to the database or update the service center information.

## Install

To use this script **require** php version 7.4 and more also you need mysql version 5.6.

To install script you need to do some steps:
1. clone this repository use next command:
```
git clone <repository>
```
2. Install php (*this step can be skipped if it is already installed*):
```
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt-get install php7.3 php7.3-bz2 php7.3-common php7.3-cgi php7.3-cli  php7.3-dba php7.3-dev libphp7.3-embed php7.3-bcmath php7.3-gmp php7.3-fpm php7.3-mysql php7.3-tidy php7.3-sqlite3 php7.3-json php7.3-sybase php7.3-curl php7.3-ldap php7.3-phpdbg php7.3-imap php7.3-xml php7.3-xsl php7.3-intl php7.3-zip php7.3-odbc php7.3-mbstring php7.3-readline php7.3-gd php7.3-interbase php7.3-xmlrpc php7.3-snmp php7.3-soap php7.3-pspell php7.3-xdebug
```
3. install composer (*this step can be skipped if it is already installed*):
```
wget -cO - https://getcomposer.org/download/1.10.19/composer.phar > /usr/local/bin/composer
chmod 755 /usr/local/bin/composer
```
4. Use the next command for install dependencies:
```
composer install
```
5. Create **.env** file copy all fields from **.env.sample** and set fields values.
6. Run app on your local machine using next command.
```
php -S localhost:port_number
```
where **port_number** is a free port number, for example 8000.

## App structure description
*The app is built using MVC*.

### Blocks
Use as current data storage and render current page to display for user.

### Controllers
Used to determine the type of request (GET or POST) and prepare data for viewing.

### Exceptions
Handling different types of exceptions

### Models
#### Data
Describes the entity

#### Resource
Describes database queries

### Router
To track possible URI using Router class. Here describe possible URI and their controllers.

### views
Contains phtml templates for different pages.

## .env
You need to make a .env file based on the .env.sample and set the fields.
```
[database]
host =
db   =
user =
pass =
charset =
```
