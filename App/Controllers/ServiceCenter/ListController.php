<?php

namespace App\Controllers\ServiceCenter;

use App\Blocks\ServiceCenter\ServiceCenterList;
use App\Models\Resource\ServiceCenters;
use App\Controllers\AbstractController;

class ListController extends AbstractController
{
    protected $serviceCenterBlock;
    protected $serviceCenterResource;

    public function __construct()
    {
        $this->serviceCenterBlock = new ServiceCenterList();
        $this->serviceCenterResource = new ServiceCenters();
    }

    public function execute(): void
    {
        $this->serviceCenterBlock
            ->setServiceCenters($this->serviceCenterResource->getServiceCenters())
            ->render('serviceCenter/service_centers');
    }
}
