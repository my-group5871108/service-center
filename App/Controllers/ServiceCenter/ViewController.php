<?php

namespace App\Controllers\ServiceCenter;

use App\Models\Data\ServiceCenter;
use App\Models\Resource\ServiceCenters;
use App\Blocks\ServiceCenter\ServiceCenterView as ViewBlock;
use App\Controllers\AbstractController;

class ViewController extends AbstractController
{
    protected $serviceCenterBlock;
    protected $serviceCenterResource;

    public function __construct()
    {
        $this->serviceCenterBlock = new ViewBlock();
        $this->serviceCenterResource = new ServiceCenters();
    }

    public function execute(): void
    {
        $isGet = $this->checkGetRequest();
        if ($isGet) {
            $this->displayServiceCenter();
        } else {
            $this->editServiceCenter();
        }
    }

    public function displayServiceCenter(): void
    {
        $id = $_GET['id'] ?? null;
        if (!$id) {
            $this->setNoRouteHeaders();
            $this->redirectTo('/404');
        }

        $this->serviceCenterBlock
            ->setServiceCenter($this->serviceCenterResource->getServiceCenterById($id))
            ->render('serviceCenter/service_center');
    }

    public function editServiceCenter(): void
    {
        $serviceCenter = new ServiceCenter();
        $serviceCenter
            ->setId($_POST['id'])
            ->setName($_POST['name'])
            ->setCountry($_POST['country'])
            ->setCity($_POST['city'])
            ->setStreet($_POST['street'])
            ->setHouse($_POST['house'])
            ->setNumberOfPlace($_POST['number_of_place']);

        $this->serviceCenterResource->updateServiceCenterById($serviceCenter);
        $this->redirectTo('/service-centers');
    }
}
