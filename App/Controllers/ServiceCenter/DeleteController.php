<?php

namespace App\Controllers\ServiceCenter;

use App\Models\Resource\ServiceCenters;
use App\Controllers\AbstractController;

class DeleteController extends AbstractController
{
    protected $serviceCenterResource;

    public function __construct()
    {
        $this->serviceCenterResource = new ServiceCenters();
    }

    public function execute(): void
    {
        $id = $_GET['id'];
        $this->serviceCenterResource->deleteServiceCenterById($id);
        $this->redirectTo('/service-centers');
    }
}
