<?php

namespace App\Controllers;

use App\Blocks\InternalServerError;

class ServerErrorController extends AbstractController
{
    protected $serverError;

    public function __construct()
    {
        $this->serverError = new InternalServerError();
    }

    public function execute(): void
    {
        $this->serverError->render('500');
    }
}
