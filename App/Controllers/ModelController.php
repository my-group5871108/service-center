<?php

namespace App\Controllers;

class ModelController extends AbstractController
{
    public function execute(): string
    {
        return 'This is controller for Models';
    }
}
