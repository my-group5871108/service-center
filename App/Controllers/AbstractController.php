<?php

namespace App\Controllers;

abstract class AbstractController
{
    abstract public function execute();

    protected function redirectTo(string $path)
    {
        header("Location: $path", true, 301);
    }

    protected function setNoRouteHeaders()
    {
        http_response_code(404);
    }

    protected function checkGetRequest(): bool
    {
        $isGet = $_SERVER['REQUEST_METHOD'] === 'GET';
        return $isGet;
    }
}
