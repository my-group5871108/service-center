<?php

namespace App\Controllers;

use App\Blocks\Master;
use App\Models\Resource\Masters;

class MasterController extends AbstractController
{
    protected $master;
    protected $mastersResource;

    public function __construct()
    {
        $this->master = new Master();
        $this->mastersResource = new Masters();
    }

    public function execute(): void
    {
        $this->master
            ->setMasters($this->mastersResource->getMasters())
            ->render('masters');
    }
}
