<?php

namespace App\Controllers;

use App\Models\Data\Brand as BrandData;
use App\Blocks\Brand as BrandBlock;
use App\Models\Resource\Brands;
use App\Models\Resource\Countries;

class BrandController extends AbstractController
{
    protected $brand;
    protected $brandsResource;
    protected $countriesResource;

    public function __construct()
    {
        $this->brand = new BrandBlock();
        $this->brandsResource = new Brands();
        $this->countriesResource = new Countries();
    }

    public function execute(): void
    {
        $isGet = $this->checkGetRequest();
        if ($isGet) {
            $this->render();
        } else {
            $this->addBrand();
        }
    }

    public function render(): void
    {
        $this->brand
            ->setBrands($this->brandsResource->getBrands())
            ->setCountries($this->countriesResource->getCountries())
            ->render('brands');
    }

    public function addBrand(): void
    {
        $brand = new BrandData();

        $brand
            ->setBrand($_POST['brand'])
            ->setCountryId($_POST['country_id']);

        $this->brandsResource->createBrands($brand);
        $this->redirectTo('/brands');
    }
}
