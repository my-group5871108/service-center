<?php

namespace App\Controllers;

use App\Blocks\Home as HomeBlock;
use App\Models\Data\Client;
use App\Models\Resource\Home as HomeResource;

class HomeController extends AbstractController
{
    protected $homeBlock;
    protected $homeResource;

    public function __construct()
    {
        $this->homeBlock = new HomeBlock();
        $this->homeResource = new HomeResource();
    }

    public function execute(): void
    {
        $isGet = $this->checkGetRequest();
        if ($isGet) {
            $this->displayHomePage();
        } else {
            $this->addClient();
        }
    }

    public function displayHomePage(): void
    {
        $this->homeBlock->render('home');
    }

    public function addClient(): void
    {
        $client = new Client();

        $client
            ->setName($_POST['name'])
            ->setSurname($_POST['surname'])
            ->setPassportId($_POST['passport_id'])
            ->setPassword($_POST['password']);

        $this->homeResource->createClients($client);
        $this->redirectTo('/');
    }
}
