<?php

namespace App\Controllers;

use App\Blocks\ServiceRegistration;
use App\Models\Resource\ServiceRegistrations;

class ServiceRegistrationController extends AbstractController
{
    protected $serviceRegistration;
    protected $serviceRegistrationResource;

    public function __construct()
    {
        $this->serviceRegistration = new ServiceRegistration();
        $this->serviceRegistrationResource = new ServiceRegistrations();
    }

    public function execute(): void
    {
        $this->serviceRegistration
            ->setServiceRegistrations($this->serviceRegistrationResource->getServiceRegistrations())
            ->render('service_registrations');
    }
}
