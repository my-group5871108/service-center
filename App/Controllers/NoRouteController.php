<?php

namespace App\Controllers;

use App\Blocks\NoRoute;

class NoRouteController extends AbstractController
{
    protected $noRoute;

    public function __construct()
    {
        $this->noRoute = new NoRoute();
    }

    public function execute(): void
    {
        $this->noRoute->render('404');
    }
}
