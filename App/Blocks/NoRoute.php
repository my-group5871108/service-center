<?php

namespace App\Blocks;

class NoRoute extends View
{
    protected $title = '404';

    public function getTitle(): string
    {
        return $this->title;
    }
}
