<?php

namespace App\Blocks;

use App\Models\Resource\Home as HomeResource;

class Home extends View
{
    protected $home;
    protected $data = ['Home page'];
    protected $title = 'Home';

    public function __construct()
    {
        $this->home = new HomeResource();
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
