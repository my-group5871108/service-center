<?php

namespace App\Blocks;

use App\Models\Resource\ServiceRegistrations;

class ServiceRegistration extends View
{
    protected $registrations = [];
    protected $serviceRegistrations;
    protected $title = 'Service Registration';

    public function __construct()
    {
        $this->serviceRegistrations = new ServiceRegistrations();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setServiceRegistrations($data): self
    {
        $this->registrations = $data;
        return $this;
    }

    public function getServiceRegistrations(): array
    {
        return $this->registrations;
    }
}
