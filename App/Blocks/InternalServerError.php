<?php

namespace App\Blocks;

class InternalServerError extends View
{
    protected $title = '500';

    public function getTitle(): string
    {
        return $this->title;
    }
}
