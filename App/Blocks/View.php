<?php

namespace App\Blocks;

abstract class View
{
    protected $template;
    protected $title = 'Сервисный центр';
    protected $links = [
        ['https://www.instagram.com', '/images/instagram.png'],
        ['https://facebook.com', '/images/facebook.png'],
        ['https://vk.com', '/images/vk.png']
    ];

    public function render($template): void
    {
        $this->template = $template;
        require APP_ROOT . "/views/main_layout.phtml";
    }

    public function getLinks(): array
    {
        return $this->links;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
