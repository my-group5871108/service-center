<?php

namespace App\Blocks;

use App\Models\Data\Country;
use App\Models\Resource\Brands;
use App\Models\Resource\Countries;

class Brand extends View
{
    protected $allBrandsData = [];
    protected $allCountriesData = [];
    protected $title = 'Brands';
    protected $brands;
    protected $countries;

    public function __construct()
    {
        $this->brands = new Brands();
        $this->countries = new Countries();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setBrands($data): self
    {
        $this->allBrandsData = $data;
        return $this;
    }

    public function getBrands(): array
    {
        return $this->allBrandsData;
    }

    public function setCountries($data): self
    {
        $this->allCountriesData = $data;
        return $this;
    }

    /**
     * @return Country[]
     */
    public function getCountries(): array
    {
        return $this->allCountriesData;
    }
}
