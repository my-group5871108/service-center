<?php

namespace App\Blocks;

use App\Models\Resource\Masters;

class Master extends View
{
    protected $allMastersData;
    protected $masters;
    protected $title = 'Masters';

    public function __construct()
    {
        $this->masters = new Masters();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setMasters($data): self
    {
        $this->allMastersData = $data;
        return $this;
    }

    public function getMasters(): array
    {
        return $this->allMastersData;
    }
}
