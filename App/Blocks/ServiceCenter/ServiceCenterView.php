<?php

namespace App\Blocks\ServiceCenter;

use App\Blocks\View;
use App\Models\Data\ServiceCenter;
use App\Models\Resource\ServiceCenters;

class ServiceCenterView extends View
{
    protected $serviceCenterData;
    protected $serviceCenter;
    protected $title = 'Service Center';

    public function __construct()
    {
        $this->serviceCenter = new ServiceCenters();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setServiceCenter($data): self
    {
        $this->serviceCenterData = $data;
        return $this;
    }


    /**
     * @return ServiceCenter[]
     */
    public function getServiceCenter(): ServiceCenter
    {
        return $this->serviceCenterData;
    }
}
