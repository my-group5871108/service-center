<?php

namespace App\Blocks\ServiceCenter;

use App\Blocks\View;
use App\Models\Resource\ServiceCenters;

class ServiceCenterList extends View
{
    protected $serviceCentersData = [];
    protected $serviceCenters;
    protected $title = 'Service Centers';

    public function __construct()
    {
        $this->serviceCenters = new ServiceCenters();
    }

    public function getTitle(): string
    {
        return $this->title;
    }


    public function setServiceCenters($data): self
    {
        $this->serviceCentersData = $data;
        return $this;
    }

    public function getServiceCenters(): array
    {
        return $this->serviceCentersData;
    }
}
