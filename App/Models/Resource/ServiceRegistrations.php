<?php

namespace App\Models\Resource;

class ServiceRegistrations extends AbstractResource
{
    public function getServiceRegistrations()
    {
        $script = 'SELECT c.name, r.date_of_recording FROM service_registrations AS r 
                   JOIN service_centers AS c ON r.service_id = c.id;';
        $stmt = $this->connection->query($script);
        return $stmt->fetchAll();
    }
}
