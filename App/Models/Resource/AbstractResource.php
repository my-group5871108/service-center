<?php

namespace App\Models\Resource;

use App\Models\Database;

abstract class AbstractResource
{
    protected $connection;

    public function __construct()
    {
        $this->connection = Database::getConnection();
    }
}
