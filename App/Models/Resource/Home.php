<?php

namespace App\Models\Resource;

use App\Exceptions\InternalServerErrorException;
use App\Models\Data\Client;
use Exception;

class Home extends AbstractResource
{
    public function createClients(Client $data)
    {
        try {
            $values = [
                $data->getName(),
                $data->getSurname(),
                $data->getPassportId(),
                $data->getPassword()
            ];

            $script = 'INSERT INTO clients (name, surname, passport_id, password) VALUES (?, ?, ?, ?)';

            $stmt = $this->connection->prepare($script);
            $stmt->execute($values);
        } catch (Exception $exception) {
            throw new InternalServerErrorException();
        }
    }
}
