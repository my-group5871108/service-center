<?php

namespace App\Models\Resource;

use App\Models\Data\Country;

class Countries extends AbstractResource
{
    public function getCountries(): array
    {
        $script = 'SELECT * FROM countries';
        $stmt = $this->connection->query($script);
        $result = $stmt->fetchAll();

        $countries = [];

        foreach ($result as $record) {
            $country = new Country();
            $country
                ->setId($record['id'])
                ->setCountry($record['country']);

            $countries[] = $country;
        }

        return $countries;
    }
}
