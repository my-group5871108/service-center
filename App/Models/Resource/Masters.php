<?php

namespace App\Models\Resource;

class Masters extends AbstractResource
{
    public function getMasters()
    {
        $script = 'SELECT l.name, l.surname, h.surname as head_surname 
                   FROM masters AS l JOIN masters AS h ON l.head_master_id = h.id;';
        $stmt = $this->connection->query($script);
        return $stmt->fetchAll();
    }
}
