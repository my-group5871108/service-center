<?php

namespace App\Models\Resource;

use App\Models\Data\Brand;

class Brands extends AbstractResource
{
    public function getBrands()
    {
        $script = 'SELECT * FROM brands JOIN countries ON brands.country_id = countries.id';
        $stmt = $this->connection->query($script);

        return $stmt->fetchAll();
    }

    public function createBrands(Brand $data)
    {
        $values = [
            $data->getBrand(),
            $data->getCountryId()
        ];

        $script = 'INSERT INTO brands (brand, country_id) VALUES (?, ?)';
        $stmt = $this->connection->prepare($script);
        $stmt->execute($values);
    }
}
