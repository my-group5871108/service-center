<?php

namespace App\Models\Resource;

use App\Exceptions\InternalServerErrorException;
use App\Exceptions\PageNotFoundException;
use App\Models\Data\ServiceCenter;
use Exception;
use PDO;

class ServiceCenters extends AbstractResource
{
    public function getServiceCenters()
    {
        $script = 'SELECT * FROM service_centers';
        $stmt = $this->connection->query($script);
        $result = $stmt->fetchAll();

        $serviceCenters = [];
        foreach ($result as $record) {
            $serviceCenter = new ServiceCenter();
            $serviceCenter
                ->setId($record['id'])
                ->setName($record['name'])
                ->setCountry($record['country'])
                ->setCity($record['city'])
                ->setStreet($record['street'])
                ->setHouse($record['house'])
                ->setNumberOfPlace($record['number_of_place']);

            $serviceCenters[] = $serviceCenter;
        }

        return $serviceCenters;
    }

    public function getServiceCenterById($id)
    {
        $script = 'SELECT * FROM service_centers WHERE id = :id';
        $stmt = $this->connection->prepare($script);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetch();

        if (!$result) {
            throw new PageNotFoundException();
        }

        $serviceCenter = new ServiceCenter();
        $serviceCenter
            ->setId($result['id'])
            ->setName($result['name'])
            ->setCountry($result['country'])
            ->setCity($result['city'])
            ->setStreet($result['street'])
            ->setHouse($result['house'])
            ->setNumberOfPlace($result['number_of_place']);

        return $serviceCenter;
    }

    public function updateServiceCenterById(ServiceCenter $serviceCenter)
    {
        try {
            $values = [
                $serviceCenter->getName(),
                $serviceCenter->getCountry(),
                $serviceCenter->getCity(),
                $serviceCenter->getStreet(),
                $serviceCenter->getHouse(),
                $serviceCenter->getNumberOfPlace(),
                $serviceCenter->getId(),
            ];

            $script = 'UPDATE service_centers SET name = ?, country = ?, city = ?, street = ?, house = ?, 
                               number_of_place = ?  WHERE id = ?';

            $stmt = $this->connection->prepare($script);
            $stmt->execute($values);
        } catch (Exception $exception) {
            throw new InternalServerErrorException();
        }
    }

    public function deleteServiceCenterById($id)
    {
        try {
            $script = 'DELETE FROM service_centers WHERE id = :id';

            $stmt = $this->connection->prepare($script);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $exception) {
            throw new InternalServerErrorException();
        }
    }
}
