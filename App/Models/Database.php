<?php

namespace App\Models;

use PDO;

class Database
{
    public static $connection;

    public static function getConnection(): PDO
    {
        if (self::$connection) {
            return self::$connection;
        }

        $envSettings = new Config();

        $host = $envSettings->getDbHost();
        $db   = $envSettings->getDbName();
        $user = $envSettings->getDbUser();
        $pass = $envSettings->getDbPass();
        $charset = $envSettings->getDbCharset();

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        self::$connection = new PDO($dsn, $user, $pass, $options);

        return self::$connection;
    }
}
