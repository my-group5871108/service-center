<?php

namespace App\Models\Data;

class Brand
{
    protected $id;
    protected $brand;
    protected $countryId;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand): self
    {
        $this->brand = $brand;
        return $this;
    }

    public function getCountryId()
    {
        return $this->countryId;
    }

    public function setCountryId($countryId): self
    {
        $this->countryId = $countryId;
        return $this;
    }
}
