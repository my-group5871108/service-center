<?php

namespace App\Models\Data;

class Client
{
    protected $id;
    protected $name;
    protected $surname;
    protected $passportId;
    protected $password;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname): self
    {
        $this->surname = $surname;
        return $this;
    }

    public function getPassportId()
    {
        return $this->passportId;
    }

    public function setPassportId($passportId): self
    {
        $this->passportId = $passportId;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;
        return $this;
    }
}
