<?php

namespace App\Models\Data;

class Country
{
    protected $id;
    protected $country;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country): self
    {
        $this->country = $country;
        return $this;
    }
}
