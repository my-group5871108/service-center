<?php

namespace App\Models\Data;

class Master
{
    protected $id;
    protected $headMasterId;
    protected $name;
    protected $surname;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getHeadMasterId()
    {
        return $this->headMasterId;
    }

    public function setHeadMasterId($headMasterId): self
    {
        $this->headMasterId = $headMasterId;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname): self
    {
        $this->surname = $surname;
        return $this;
    }
}
