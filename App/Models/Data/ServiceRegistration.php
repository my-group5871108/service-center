<?php

namespace App\Models\Data;

class ServiceRegistration
{
    protected $id;
    protected $masterId;
    protected $carId;
    protected $clientId;
    protected $dateOfRecording;
    protected $serviceId;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getMasterId()
    {
        return $this->masterId;
    }

    public function setMasterId($masterId): self
    {
        $this->masterId = $masterId;
        return $this;
    }

    public function getCarId()
    {
        return $this->carId;
    }

    public function setCarId($carId): self
    {
        $this->carId = $carId;
        return $this;
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    public function setClientId($clientId): self
    {
        $this->clientId = $clientId;
        return $this;
    }

    public function getDateOfRecording()
    {
        return $this->dateOfRecording;
    }

    public function setDateOfRecording($dateOfRecording): self
    {
        $this->dateOfRecording = $dateOfRecording;
        return $this;
    }

    public function getServiceId()
    {
        return $this->serviceId;
    }

    public function setServiceId($serviceId): self
    {
        $this->serviceId = $serviceId;
        return $this;
    }
}
