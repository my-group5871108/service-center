<?php

namespace App\Models\Data;

class ServiceCenter
{
    protected $id;
    protected $name;
    protected $country;
    protected $city;
    protected $street;
    protected $house;
    protected $numberOfPlace;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country): self
    {
        $this->country = $country;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city): self
    {
        $this->city = $city;
        return $this;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setStreet($street): self
    {
        $this->street = $street;
        return $this;
    }

    public function getHouse()
    {
        return $this->house;
    }

    public function setHouse($house): self
    {
        $this->house = $house;
        return $this;
    }

    public function getNumberOfPlace()
    {
        return $this->numberOfPlace;
    }

    public function setNumberOfPlace($numberOfPlace): self
    {
        $this->numberOfPlace = $numberOfPlace;
        return $this;
    }
}
