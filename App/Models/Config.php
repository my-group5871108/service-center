<?php

namespace App\Models;

class Config
{
    protected const DB_HOST_KEY = 'host';
    protected const DB_NAME_KEY = 'db';
    protected const DB_USER_KEY = 'user';
    protected const DB_PASS_KEY = 'pass';
    protected const DB_CHARSET_KEY = 'charset';

    protected const GROUP = 'database';
    protected const DEFAULT_HOST = '127.0.0.1';
    protected const DEFAULT_CHARSET = 'utf8mb4';
    protected const FILE_ENVIRONMENT = '/.env';

    protected $envSettings = [];

    public function __construct()
    {
        if (!$this->envSettings) {
            $this->envSettings = parse_ini_file(APP_ROOT . self::FILE_ENVIRONMENT, true);
        }
    }

    public function getDbHost()
    {
        return $this->envSettings[self::GROUP][self::DB_HOST_KEY] ?? self::DEFAULT_HOST;
    }

    public function getDbName()
    {
        return $this->envSettings[self::GROUP][self::DB_NAME_KEY];
    }

    public function getDbUser()
    {
        return $this->envSettings[self::GROUP][self::DB_USER_KEY];
    }

    public function getDbPass()
    {
        return $this->envSettings[self::GROUP][self::DB_PASS_KEY];
    }

    public function getDbCharset()
    {
        return $this->envSettings[self::GROUP][self::DB_CHARSET_KEY] ?? self::DEFAULT_CHARSET;
    }
}
