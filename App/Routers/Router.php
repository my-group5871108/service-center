<?php

namespace App\Routers;

use App\Controllers\AbstractController;
use App\Controllers\BrandController;
use App\Controllers\CarController;
use App\Controllers\ClientController;
use App\Controllers\CountryController;
use App\Controllers\HomeController;
use App\Controllers\MasterController;
use App\Controllers\ModelController;
use App\Controllers\NoRouteController;
use App\Controllers\ServiceCenter\DeleteController;
use App\Controllers\ServiceCenter\ListController;
use App\Controllers\ServiceCenter\ViewController;
use App\Controllers\ServiceRegistrationController;

class Router
{
    protected function getRequestPath($requestPath): string
    {
        $requestParamsPosition = strpos($requestPath, '?');
        if ($requestParamsPosition !== false) {
            $requestPath = substr($requestPath, 0, $requestParamsPosition);
        }
        return $requestPath;
    }

    public function processRequest(string $requestPath): AbstractController
    {
        $requestPath = $this->getRequestPath($requestPath);

        switch ($requestPath) {
            case '/':
                return new HomeController();
            case '/brands':
                return new BrandController();
            case '/cars':
                return new CarController();
            case '/clients':
                return new ClientController();
            case '/countries':
                return new CountryController();
            case '/masters':
                return new MasterController();
            case '/models':
                return new ModelController();
            case '/service-centers':
                return new ListController();
            case '/service-center':
                return new ViewController();
            case '/service-center/delete':
                return new DeleteController();
            case '/service-registrations':
                return new ServiceRegistrationController();
            default:
                return new NoRouteController();
        }
    }
}
